package br.com.pupposoft.poc.spring.kafka.gateways.http.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pupposoft.poc.spring.kafka.gateways.http.controllers.json.ProducerRequestJson;
import br.com.pupposoft.poc.spring.kafka.gateways.queue.Producer;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("producer")
public class ProducerController {
	
	@Autowired
	private Producer producer;
	
	@PostMapping
	public String create(@RequestBody(required = true) final ProducerRequestJson producerRequestJson) {
		
		this.producer.send(producerRequestJson.getTopic(), producerRequestJson.getPayload());
		
		return "Send OK!";
	}

}
