package br.com.pupposoft.poc.spring.kafka.gateways.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ProducerRequestJson {
	private String topic;
	private String payload;
}