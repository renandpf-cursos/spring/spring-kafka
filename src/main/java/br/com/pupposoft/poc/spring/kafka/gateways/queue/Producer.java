package br.com.pupposoft.poc.spring.kafka.gateways.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

@Component
public class Producer {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void send(String topic, String payload) {
    	
    	
        kafkaTemplate.send(topic, payload).addCallback(new SuccessCallback<Object>() {

				@Override
				public void onSuccess(Object result) {
					System.out.println("Sucess!: " + result);
					
				}
			},
            	new FailureCallback() {
					@Override
					public void onFailure(Throwable ex) {
						ex.printStackTrace();
					}
				});
    }

}
